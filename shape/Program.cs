﻿using OpenCvSharp;
using System;
using System.Collections.Generic;

namespace shape {
	class Program {
		enum ArgIndex : byte { INPUT = 1, OUTPUT = 2, DST = 3, SRC = 4, HELP = 5 }
		public struct Info {
			public string input, output;
			public Point2d[] src, dst;
			public Info(string i, string o, Point2d[] s, Point2d[] d) {
				input = i;
				output = o;
				src = s;
				dst = d;
			}
		}

		static string[] GetArgsFromKeyboard() {
			string line = Console.ReadLine();
			List<string> values = new List<string>();
			int idx = 0;
			bool pass = false;
			string value = "";
			while(idx <= line.Length) {
				if(idx == line.Length) {
					values.Add(value);
					value = "";
					idx++;
					continue;
				} else if(!pass && line[idx] == ' ') {
					values.Add(value);
					value = "";
					idx++;
					continue;
				} else if(!pass && line[idx] == '"') {
					pass = true;
					idx++;
				} else if(pass && line[idx] == '"') {
					pass = false;
					values.Add(value);
					value = "";
					idx += 2;
					continue;
				}
				value += line[idx++];
			}

			return values.ToArray();
		}

		static void PrintHelpEXIT() {
			Console.WriteLine("\nshape.exe -i src -o res -s [x1 y1 x2 y2 x3 y3 x4 y4] -d [width height]\n");
			Console.WriteLine("\t-i/--input : source image file");
			Console.WriteLine("\t-o/--output : result image file");
			Console.WriteLine("\t-s/--source : the rectangle(4 points) to transform from source image");
			Console.WriteLine("\t\t4 points must be entered clockwise from the top left corner.");
			Console.WriteLine("\t-d/--destination : the result image size (width and height)");
			Console.WriteLine("\t-h/--help : show help");
			Console.WriteLine("\nThis prog. makes the rectangle area a flat rectangle.");
			Console.WriteLine("ex) shape.exe -i src.jpg -o res.jpg -s 0 0 1 0 1 1 0 1 -d 640 480 \n");
			Environment.Exit(0);
		}

		static Info ArgParser(string[] args) {
			int i = 0;

			string input = "", output = "";
			Point2d[] src = new Point2d[4];
			Point2d[] dst = new Point2d[4];

			while(i < args.Length) {
				int val;
				if(!argVal.TryGetValue(args[i++], out val))
					PrintHelpEXIT();

				try {
					switch(val) {
						case (int)ArgIndex.INPUT:
							input = args[i++];
							break;
						case (int)ArgIndex.OUTPUT:
							output = args[i++];
							break;
						case (int)ArgIndex.SRC:
							for(int k = 0; k < 4; k++) {
								double x = 0, y = 0;
								x = double.Parse(args[i++]);
								y = double.Parse(args[i++]);
								src[k] = new Point2d(x, y);
							}
							break;
						case (int)ArgIndex.DST:
							double w = 0, h = 0;
							w = double.Parse(args[i++]);
							h = double.Parse(args[i++]);
							dst[0] = new Point2d(0, 0);
							dst[1] = new Point2d(w, 0);
							dst[2] = new Point2d(w, h);
							dst[3] = new Point2d(0, h);
							break;
						case (int)ArgIndex.HELP:
						default:
							PrintHelpEXIT();
							break;

					}
				} catch(Exception e) {
					PrintHelpEXIT();
				}
			}

			return new Info(input, output, src, dst);
		}

		static Dictionary<string, int> argVal = new Dictionary<string, int>() {
			{ "-i", (int)ArgIndex.INPUT }, { "--input", (int)ArgIndex.INPUT },
			{ "-o", (int)ArgIndex.OUTPUT }, { "--output", (int)ArgIndex.OUTPUT },
			{ "-d", (int)ArgIndex.DST }, { "--destination", (int)ArgIndex.DST },
			{ "-s", (int)ArgIndex.SRC }, { "--source", (int)ArgIndex.SRC },
			{ "-h", (int)ArgIndex.HELP }, { "--help", (int)ArgIndex.HELP }
		};
		
		static void Main(string[] args) {
			if(Console.CursorLeft == 0 && Console.CursorTop == 0) {
				args = GetArgsFromKeyboard();
			}
			Info info = ArgParser(args);

			Mat T = Cv2.FindHomography(info.src, info.dst, HomographyMethods.Ransac);
			Mat srcImg = Cv2.ImRead(info.input);
			Mat resImg = new Mat();
			Cv2.WarpPerspective(srcImg, resImg, T, new Size(info.dst[2].X, info.dst[2].Y));

			if(Cv2.ImWrite(info.output, resImg)) {
				Console.WriteLine("file \"{0}\" save succeeded.", info.output);
			}else {
				Console.WriteLine("file save failed.", info.output);
			}
		}
	}
}
