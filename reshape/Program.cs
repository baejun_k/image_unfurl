﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace reshape {
	class Program {
		enum ArgIndex : byte { INPUT = 1, OUTPUT = 2, HELP = 3 }
		public struct Info {
			public string input, output;
			public Info(string i, string o) {
				input = i;
				output = o;
			}
		}

		static string[] GetArgsFromKeyboard() {
			string line = Console.ReadLine();
			List<string> values = new List<string>();
			int idx = 0;
			bool pass = false;
			string value = "";
			while (idx <= line.Length) {
				if(idx == line.Length) {
					values.Add(value);
					value = "";
					idx++;
					continue;
				} else if(!pass && line[idx] == ' ') {
					values.Add(value);
					value = "";
					idx++;
					continue;
				} else if(!pass && line[idx] == '"') {
					pass = true;
					idx++;
				} else if(pass && line[idx] == '"') {
					pass = false;
					values.Add(value);
					value = "";
					idx += 2;
					continue;
				}
				value += line[idx++];
			}

			return values.ToArray();
		}

		static void PrintHelpEXIT() {
			Console.WriteLine("\nreshape.exe -i src -o res\n");
			Console.WriteLine("\t-i/--input : source image file");
			Console.WriteLine("\t-o/--output : result image file");
			Console.WriteLine("\t-h/--help : show help");
			Environment.Exit(0);
		}

		static Info ArgParser(string[] args) {
			int i = 0;

			string input = "", output = "";

			while(i < args.Length) {
				int val;
				if(!argVal.TryGetValue(args[i++], out val))
					PrintHelpEXIT();

				try {
					switch(val) {
						case (int)ArgIndex.INPUT:
							input = args[i++];
							break;
						case (int)ArgIndex.OUTPUT:
							output = args[i++];
							break;
						case (int)ArgIndex.HELP:
						default:
							PrintHelpEXIT();
							break;

					}
				} catch(Exception e) {
					PrintHelpEXIT();
				}
			}

			return new Info(input, output);
		}

		static Dictionary<string, int> argVal = new Dictionary<string, int>() {
			{ "-i", (int)ArgIndex.INPUT }, { "--input", (int)ArgIndex.INPUT },
			{ "-o", (int)ArgIndex.OUTPUT }, { "--output", (int)ArgIndex.OUTPUT },
			{ "-h", (int)ArgIndex.HELP }, { "--help", (int)ArgIndex.HELP }
		};

		static void Main(string[] args) {
			if(Console.CursorLeft == 0 && Console.CursorTop == 0) {
				args = GetArgsFromKeyboard();
			}
			Info info = ArgParser(args);

			int[,] points = new int[2,4];

			using(var proc = new Process()) {
				proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
				proc.StartInfo.UseShellExecute = false;
				proc.StartInfo.FileName = ".\\3rdparty\\getfourpoints.exe";
				proc.StartInfo.Arguments = string.Format("-i \"{0}\" -o .\\fourpoints.txt", info.input);
				proc.Start();
				proc.WaitForExit();
				using(var sr = new System.IO.StreamReader(".\\fourpoints.txt")) {
					var line = sr.ReadLine();
					var vals = line.Split(' ');
					for(int i = 0; i < 4; i++) {
						points[0, i] = int.Parse(vals[(i * 2)]);
						points[1, i] = int.Parse(vals[(i * 2) + 1]);
					}
				}
				System.IO.File.Delete(".\\fourpoints.txt");
			}
			
			double xl1 = Math.Pow(points[0, 0] - points[0, 1], 2) + Math.Pow(points[1, 0] - points[1, 1], 2);
			double xl2 = Math.Pow(points[0, 2] - points[0, 3], 2) + Math.Pow(points[1, 2] - points[1, 3], 2);
			double yl1 = Math.Pow(points[0, 1] - points[0, 2], 2) + Math.Pow(points[1, 1] - points[1, 2], 2);
			double yl2 = Math.Pow(points[0, 0] - points[0, 3], 2) + Math.Pow(points[1, 0] - points[1, 3], 2);

			int w = (int)Math.Sqrt(Math.Max(xl1, xl2));
			int h = (int)Math.Sqrt(Math.Max(yl1, yl2));

			if(w <= 0 || h <= 0) {
				Console.WriteLine("Some error");
				return;
			}

			using(var proc = new Process()) {
				proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
				proc.StartInfo.UseShellExecute = false;
				proc.StartInfo.FileName = ".\\3rdparty\\shape.exe";
				proc.StartInfo.Arguments = string.Format("-i \"{0}\" -o \"{1}\" -s {2} {3} {4} {5} {6} {7} {8} {9} -d {10} {11}", 
					info.input, info.output, 
					points[0, 0], points[1, 0], points[0, 1], points[1, 1], points[0, 2], points[1, 2], points[0, 3], points[1, 3], 
					w, h);
				proc.Start();
				proc.WaitForExit();
			}
		}
	}
}
