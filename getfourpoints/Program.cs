﻿using System;
using System.Collections.Generic;
using OpenCvSharp;

namespace getfourpoints {
	class Program {
		enum ArgIndex : byte { INPUT = 1, OUTPUT = 2, HELP = 3 }
		public struct Info {
			public string input, output;
			public Info(string i, string o) {
				input = i;
				output = o;
			}
		}

		static string[] GetArgsFromKeyboard() {
			string line = Console.ReadLine();
			List<string> values = new List<string>();
			int idx = 0;
			bool pass = false;
			string value = "";
			while(idx <= line.Length) {
				if(idx == line.Length) {
					values.Add(value);
					value = "";
					idx++;
					continue;
				} else if(!pass && line[idx] == ' ') {
					values.Add(value);
					value = "";
					idx++;
					continue;
				} else if(!pass && line[idx] == '"') {
					pass = true;
					idx++;
				} else if(pass && line[idx] == '"') {
					pass = false;
					values.Add(value);
					value = "";
					idx += 2;
					continue;
				}
				value += line[idx++];
			}

			return values.ToArray();
		}

		static void PrintHelpEXIT() {
			Console.WriteLine("\ngetfourpoints.exe -i src -o res\n");
			Console.WriteLine("\t-i/--input : source image file");
			Console.WriteLine("\t-o/--output : result txt file");
			Console.WriteLine("\t-h/--help : show help");
			Console.WriteLine("\nThis prog. gets four points of the rectangle.");
			Console.WriteLine("Take four points clockwise from the top left corner.");
			Console.WriteLine("ex) getfourpoints.exe -i src.jpg -o res.jpg\n");
			Environment.Exit(0);
		}

		static Info ArgParser(string[] args) {
			int i = 0;

			string input = "", output = "";
			Point2d[] src = new Point2d[4];
			Point2d[] dst = new Point2d[4];

			while(i < args.Length) {
				int val;
				if(!argVal.TryGetValue(args[i++], out val))
					PrintHelpEXIT();

				try {
					switch(val) {
						case (int)ArgIndex.INPUT:
							input = args[i++];
							break;
						case (int)ArgIndex.OUTPUT:
							output = args[i++];
							break;
						case (int)ArgIndex.HELP:
						default:
							PrintHelpEXIT();
							break;

					}
				} catch(Exception e) {
					PrintHelpEXIT();
				}
			}

			return new Info(input, output);
		}

		static Dictionary<string, int> argVal = new Dictionary<string, int>() {
			{ "-i", (int)ArgIndex.INPUT }, { "--input", (int)ArgIndex.INPUT },
			{ "-o", (int)ArgIndex.OUTPUT }, { "--output", (int)ArgIndex.OUTPUT },
			{ "-h", (int)ArgIndex.HELP }, { "--help", (int)ArgIndex.HELP }
		};

		static void Main(string[] args) {
			if(Console.CursorLeft == 0 && Console.CursorTop == 0) {
				args = GetArgsFromKeyboard();
			}
			Info info = ArgParser(args);

			Mat srcImg = Cv2.ImRead(info.input);
			Mat view = new Mat();
			Cv2.Resize(srcImg, view, new Size(640, 480));

			int rectIdx = 0;
			Point[] RectPts = new Point[4];
			RectPts.Initialize();
			Point[] reRectPts = new Point[4];
			reRectPts.Initialize();

			OpenCvSharp.Window win = new Window("input image", WindowMode.AutoSize);

			win.SetMouseCallback((evt, x, y, flag, data) => {
				Cv2.Resize(srcImg, view, new Size(640, 480));

				double u = x / 640.0;
				double v = y / 480.0;

				if(evt == MouseEvent.LButtonDown) {
					if(rectIdx == 4)
						rectIdx = 0;
					else {
						RectPts[rectIdx].X = (int)Math.Round(srcImg.Cols * u);
						RectPts[rectIdx].Y = (int)Math.Round(srcImg.Rows * v);
						reRectPts[rectIdx].X = x;
						reRectPts[rectIdx].Y = y;
						rectIdx++;
					}
				}

				for(int i = 0; i < rectIdx; i++) {
					Cv2.Circle(view, reRectPts[i], 3, new Scalar(0, 0, 255), 2);
					Cv2.Line(view, reRectPts[i], reRectPts[(i + 1) % rectIdx], new Scalar(0, 0, 255), 2);
				}
				GC.Collect();
			});

			bool run = true;
			while(run) {
				win.ShowImage(view);
				switch(Cv2.WaitKey(33)) {
					case 27:
						run = false;
						Console.WriteLine("quit.");
						break;
					case 13:
						if(rectIdx == 4) {
							using(var fs = System.IO.File.Open(info.output, System.IO.FileMode.Create))
							using(var sw = new System.IO.StreamWriter(fs)) {
								foreach(var p in RectPts)
									sw.Write("{0} {1} ", p.X, p.Y);
							}
							Console.WriteLine("successed.");
							return;
						}
						Console.WriteLine("Need more points({0}/4 points). press esc to leave.");
						break;
				}
			}

		}
	}
}
